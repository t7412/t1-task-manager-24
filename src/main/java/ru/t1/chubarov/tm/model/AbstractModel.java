package ru.t1.chubarov.tm.model;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public abstract class AbstractModel {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

}
